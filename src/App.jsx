import React from 'react';
import Game from './components/Game';

import './index.css';

/** The main component. */
const App = () => {
  return (
    <div id='wrapper'>
      <Game />
    </div>
  );
};

export default App;
